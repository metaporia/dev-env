# TODO
□  switch out bash for fish.
□  add dotfiles   
□  layer per language    
    □  rust dev env: cargo   
    □  haskell: stack, ghcmod, hlint, etc..   
    □  c/c++: gcc | clang (??)   
□  dictionary/gcide: dicod-docker integration (see docker-compose)    
□  cli which accepts git repo url, and/or prog. lang. and opens fresh container    
   with the deps and dev-env as indicated by a repo-specific Dockerfile or some    
   such instruction set.   
    □ allow user to specify:   
        □  editor    
        □  programming language   
        □  git repo[s]   
        □  containerized services, e.g., dicod-docker, or a gitlab instance,   
           etc.   

