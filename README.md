# README

## Build container

```bash
cd /path/to/repo/dev-env
docker build -t dev . 
```

## Instantiate
```bash
docker run -it --rm dev bash
```
