# instructions @ https://github.com/AGhost-7/docker-dev/tree/master/tutorial
FROM ubuntu:xenial

RUN    apt-get update \
    && apt-get install -y sudo

ENV USER aporia

# no password for user
# no password for sudo
RUN    adduser --disabled-password --gecos '' "$USER" \
    && adduser "$USER" sudo \ 
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER "$USER"

WORKDIR "/home/$USER"

# obviate "that sudo message"
RUN touch ~/.sudo_as_admin_successful


## base deps 
# pls try to keep them alphabetized.
RUN    sudo apt-get update \
    && sudo apt-get install -y \
       bash-completion \
       build-essential \
       curl \
       git \
       locate \
       man-db \
       openssh-client

# run via: docker run --rm -it <container-id> bash

## neovim
RUN    sudo apt-get install -y software-properties-common \
    && sudo add-apt-repository ppa:neovim-ppa/unstable \
    && sudo apt-get update \
    && sudo apt-get install -y neovim \
    && mkdir -p "$HOME/.config/nvim"

COPY ./init.vim /tmp/init.vim
RUN cat /tmp/init.vim > ~/.config/nvim/init.vim && \
    sudo rm /tmp/init.vim


RUN curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

#RUN nvim +PlugInstall +qall
#+UpdateRemotePlugins +qall

ENV EDITOR nvim

ENV TMUX_VERSION 2.6
ENV TMUX_TAR "tmux-$TMUX_VERSION.tar.gz"


# TODO: place copy of tar in dev-env repo, i.e., reduce external
# dependencies--the workflow should consist of cloning dev-env, building and
# running the container.

# Download the tmux archive
RUN curl -L -o "/tmp/tmux-$TMUX_VERSION.tar.gz" \
		"https://github.com/tmux/tmux/releases/download/$TMUX_VERSION/$TMUX_TAR"

# Change our working directory to the location where our archive is
WORKDIR /tmp

# Untar the tmux source code
RUN tar xzf "$TMUX_TAR" -C /tmp

# Switch to the directory containing the extracted source code.
WORKDIR "/tmp/tmux-$TMUX_VERSION"

# Since we're building source code, we will require certain libraries to
# compiler against (header files) as well as library files which will be
# linked to the tmux program at runtime.
RUN sudo apt-get install -y libevent-2.0-5 libevent-dev libncurses-dev

# Generate configuration files and make sure all dependencies are present
RUN    ./configure \
    && make \
    && sudo make install


# Tmux requires the TERM environment variable to be set to this specific value
# to run as one would expect.
ENV TERM=screen-256color

# Switch back to our normal directory
WORKDIR "/home/$USER"

# wtf doesn't he just copy conf files to their final destination??! FIX
# Copy our basic tmux configuration
COPY ./.tmux.conf /tmp/.tmux.conf
RUN cat /tmp/.tmux.conf > ~/.tmux.conf && \
	sudo rm /tmp/.tmux.conf


# for deoplete, etc.
RUN sudo apt-get install -y python3-pip && \
    pip3 install --upgrade && \
    pip3 install neovim 

RUN nvim --headless +PlugInstall +qall

