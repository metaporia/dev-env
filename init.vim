call plug#begin('~/.local/share/nvim/plugged')
Plug 'vim-scripts/Sift'
Plug 'neomake/neomake'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'jreybert/vimagit'
Plug 'chriskempson/base16-vim'
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }  
call plug#end()

inoremap jk <Esc>
let mapleader =','
nnoremap <leader>w :w<CR>
nnoremap <leader>r :source ~/.vimrc<CR>

inoremap <C-f> <Right>
inoremap <C-b> <Left>
inoremap <C-a> <Home>
inoremap <C-e> <End>

set number
set relativenumber

set hidden
set ignorecase
se nobackup
se noswapfile
se nowritebackup

se smartcase
se smartindent

se mouse=a

se timeout
" mapping timeout
se timeoutlen=200
" keycode timeout
se ttimeoutlen=50

se tw=79

se breakindent
se noshowmode

let loaded_matchparen = 0
se lazyredraw
set wildignorecase

"show status line: always
set laststatus=2
"clear (if any) pre-existing value of 'statusline'
set statusline=
"far left; items accumlate rightwards
"time
set statusline=%{strftime(\"%m-%d\ [%H:%M]\")}
"full file path
set statusline+=\ %F\ 
"filetype, [RO] (opt), buf num
set statusline+=%y\ %r\ buf:[%n]
"FAR RIGHT; items accumulate leftwards
set statusline+=%=

"se cursorline

se number
se relativenumber
set pastetoggle=<F3>

"sessions
set sessionoptions+=options,globals,curdir,buffers,help,winsize,

"sys clip
"append motion to yank
"yank to sys clip
nnoremap <leader>c "+y
vnoremap <leader>c "+y

"yank to sys sel1
nnoremap <leader>s "*y
vnoremap <leader>s "*y

"read sys clip
nnoremap <leader>p "+p
"read sys sel1
nnoremap <leader>is "*p

"insert formatted date cmd
nnoremap <leader>ia "=strftime(" %g/%m/%d/%H/%M/%S")<CR>P

nnoremap <leader>it "=strftime("%H:%M:%S")<CR>P

" log w style
nnoremap <leader>t Go<C-r>=strftime("%H:%M:%S λ. ")<CR>

" Magit ldr
nnoremap <leader>M :Magit<CR>

" Insert Firefox Active Tab URL
nnoremap <leader>u :r !factab<CR>

"buffer nav
"buff next
nnoremap <c-n> :bn<CR>

"buff prev
nnoremap <c-p> :bp<CR>

"buf kill
nnoremap <c-k> :bd<CR>

"list buffers
nnoremap <leader>l :ls<CR>

if has("termguicolors")
    set termguicolors
endif

" fix LineNr bg/fg contrast probem
function! MatchLineNrBgToGuibg()
    let main_bg_id=hlID('Normal')
    let main_guibg=synIDattr(main_bg_id, 'bg#', "gui")
    highlight LineNr guibg=main_guibg
endfunction

" set Hl-search to match IncSearch
function! SetSearchColorToIncSearch()
    let inc_search_id=hlID('IncSearch')
    let incs_bg=synIDattr(inc_search_id, 'bg#', "gui")
    let incs_fg=synIDattr(inc_search_id, 'fg#', "gui")
    execute 'highlight Search guibg=' . incs_bg . ' guifg=' . incs_fg 
endfunction

augroup FixSearchColor
    au!
    au ColorScheme * call SetSearchColorToIncSearch()
    au ColorScheme * call MatchLineNrBgToGuibg()
augroup END


set listchars=
if (&termencoding ==# 'utf-8' || &encoding ==# 'utf-8') && version >= 700
    " | mulibyte
    set listchars=eol:¶,space:.,tab:\|·,trail:·,extends:»,precedes:«
else
    set listchars=eol:$
endif


"reload .vimrc 
nnoremap <leader>r :source ~/.config/nvim/init.vim<CR>:echo "reloaded init.vim"<CR> 

"write
nnoremap <leader>w :w<CR>

"overwrite defaulte session "./sesh"
nnoremap <leader>s :mks! sesh<CR>

" muse 
nnoremap <leader>md :new <CR>:r !muse.do<CR>:set ft=vim<CR>gg

"make
nnoremap <leader>e :silent make build \| cw<CR>

" neomake
nnoremap <leader>n :Neomake<CR>

" path 
set path=.,/usr/include,~/ws,~/sputum,~/dot,~/Axiom,~/wiki


"tabs
se ts=4
set shiftwidth=0
set softtabstop=4
set expandtab
set smarttab "needs paste toggle to insert (>1) line excerpts 

" correct misspelled word to first available correction.
nnoremap <leader>z 1z=
" toggles
"spell
nnoremap <leader>vs :set spell!<CR>
nnoremap <leader>vh :set hlsearch!<CR>
nnoremap <leader>vl :set list!<CR>
nnoremap <leader>vr :set relativenumber!<CR>



" checkbox
" <leader>b : insert '□  ' | replace w ▣
function! CheckBox() 
    let char = matchstr(getline('.'), '\%' . col('.') . 'c.')
    if char == "□"
        echo "matched!"
        execute "normal r▣"
    elseif char == "▣"
        execute "normal r□"
    else
        echo "inserted checkbox"
        execute "normal i□  "
    endif
endfunction

nnoremap <leader>b :call CheckBox()<CR>

function! Define(word, ...)
    if a:0 > 0 " a:1 contains search strategy, see ```man dico``` or ```dico --help```
        let query = "dico -s " . a:1 . " -d* " . '"' . a:word . '"'
    else
        let query = "dico -d* " . '"' . a:word . '"' . ' | fmt' 
    endif
    echo query
    " surmise
    let definitions = system(query) 
    if definitions == "dict (client_read_status): Error reading from socket\nclient_read_status: Success\n"
        "echo "error"
        let remote_query = "dict --host gnu.org.ua " . '"' . a:word . '"' . ' | fmt'
        let definitions = system(remote_query)
    endif
    silent call DeadBuf() | call bufname("dico") | silent put =definitions | normal ggdd 
    "call DeadBuf() | 
endfunction


au! VimEnter * AirlineRefresh
